SOURCEDIR := $(abspath $(patsubst %/,%,$(dir $(abspath $(lastword \
	$(MAKEFILE_LIST))))))

CC ?= cc
CXX ?= c++
CXXFLAGS ?= -O2
FLAGS := -fPIC -std=c++98
DEFS := -DNST_PRAGMA_ONCE -DNST_NO_ZLIB

PKGCONF ?= pkg-config
CFLAGS_JG := $(shell $(PKGCONF) --cflags jg)

LIBS := -lm -lstdc++
SHARED := -fPIC

NAME := nestopia
PREFIX ?= /usr/local
LIBDIR ?= $(PREFIX)/lib
DATAROOTDIR ?= $(PREFIX)/share
DATADIR ?= $(DATAROOTDIR)
DOCDIR ?= $(DATAROOTDIR)/doc/$(NAME)

UNAME := $(shell uname -s)
ifeq ($(UNAME), Darwin)
	SHARED += -dynamiclib
	TARGET := $(NAME).dylib
else ifeq ($(OS), Windows_NT)
	SHARED += -shared
	TARGET := $(NAME).dll
else
	SHARED += -shared
	TARGET := $(NAME).so
endif

ifeq ($(UNAME), Linux)
	LIBS += -Wl,--no-undefined
endif

OBJDIR := objs

# Core
CXXSRCS := $(OBJDIR)/core/NstApu.cpp \
	$(OBJDIR)/core/NstAssert.cpp \
	$(OBJDIR)/core/NstCartridge.cpp \
	$(OBJDIR)/core/NstCartridgeInes.cpp \
	$(OBJDIR)/core/NstCartridgeRomset.cpp \
	$(OBJDIR)/core/NstCartridgeUnif.cpp \
	$(OBJDIR)/core/NstCheats.cpp \
	$(OBJDIR)/core/NstChecksum.cpp \
	$(OBJDIR)/core/NstChips.cpp \
	$(OBJDIR)/core/NstCore.cpp \
	$(OBJDIR)/core/NstCpu.cpp \
	$(OBJDIR)/core/NstCrc32.cpp \
	$(OBJDIR)/core/NstFds.cpp \
	$(OBJDIR)/core/NstFile.cpp \
	$(OBJDIR)/core/NstHomebrew.cpp \
	$(OBJDIR)/core/NstImage.cpp \
	$(OBJDIR)/core/NstImageDatabase.cpp \
	$(OBJDIR)/core/NstLog.cpp \
	$(OBJDIR)/core/NstMachine.cpp \
	$(OBJDIR)/core/NstMemory.cpp \
	$(OBJDIR)/core/NstNsf.cpp \
	$(OBJDIR)/core/NstPatcher.cpp \
	$(OBJDIR)/core/NstPatcherIps.cpp \
	$(OBJDIR)/core/NstPatcherUps.cpp \
	$(OBJDIR)/core/NstPins.cpp \
	$(OBJDIR)/core/NstPpu.cpp \
	$(OBJDIR)/core/NstProperties.cpp \
	$(OBJDIR)/core/NstRam.cpp \
	$(OBJDIR)/core/NstSha1.cpp \
	$(OBJDIR)/core/NstSoundPcm.cpp \
	$(OBJDIR)/core/NstSoundPlayer.cpp \
	$(OBJDIR)/core/NstSoundRenderer.cpp \
	$(OBJDIR)/core/NstState.cpp \
	$(OBJDIR)/core/NstStream.cpp \
	$(OBJDIR)/core/NstTracker.cpp \
	$(OBJDIR)/core/NstTrackerMovie.cpp \
	$(OBJDIR)/core/NstTrackerRewinder.cpp \
	$(OBJDIR)/core/NstVector.cpp \
	$(OBJDIR)/core/NstVideoFilterNone.cpp \
	$(OBJDIR)/core/NstVideoFilterNtsc.cpp \
	$(OBJDIR)/core/NstVideoFilterNtscCfg.cpp \
	$(OBJDIR)/core/NstVideoRenderer.cpp \
	$(OBJDIR)/core/NstVideoScreen.cpp \
	$(OBJDIR)/core/NstXml.cpp \
	$(OBJDIR)/core/NstZlib.cpp \
	$(OBJDIR)/core/api/NstApiBarcodeReader.cpp \
	$(OBJDIR)/core/api/NstApiCartridge.cpp \
	$(OBJDIR)/core/api/NstApiCheats.cpp \
	$(OBJDIR)/core/api/NstApiDipSwitches.cpp \
	$(OBJDIR)/core/api/NstApiEmulator.cpp \
	$(OBJDIR)/core/api/NstApiFds.cpp \
	$(OBJDIR)/core/api/NstApiHomebrew.cpp \
	$(OBJDIR)/core/api/NstApiInput.cpp \
	$(OBJDIR)/core/api/NstApiMachine.cpp \
	$(OBJDIR)/core/api/NstApiMovie.cpp \
	$(OBJDIR)/core/api/NstApiNsf.cpp \
	$(OBJDIR)/core/api/NstApiRewinder.cpp \
	$(OBJDIR)/core/api/NstApiSound.cpp \
	$(OBJDIR)/core/api/NstApiTapeRecorder.cpp \
	$(OBJDIR)/core/api/NstApiUser.cpp \
	$(OBJDIR)/core/api/NstApiVideo.cpp \
	$(OBJDIR)/core/board/NstBoardAcclaimMcAcc.cpp \
	$(OBJDIR)/core/board/NstBoardAction53.cpp \
	$(OBJDIR)/core/board/NstBoardAe.cpp \
	$(OBJDIR)/core/board/NstBoardAgci.cpp \
	$(OBJDIR)/core/board/NstBoardAveD1012.cpp \
	$(OBJDIR)/core/board/NstBoardAveNina.cpp \
	$(OBJDIR)/core/board/NstBoardAxRom.cpp \
	$(OBJDIR)/core/board/NstBoardBandai24c0x.cpp \
	$(OBJDIR)/core/board/NstBoardBandaiAerobicsStudio.cpp \
	$(OBJDIR)/core/board/NstBoardBandaiDatach.cpp \
	$(OBJDIR)/core/board/NstBoardBandaiKaraokeStudio.cpp \
	$(OBJDIR)/core/board/NstBoardBandaiLz93d50.cpp \
	$(OBJDIR)/core/board/NstBoardBandaiLz93d50ex.cpp \
	$(OBJDIR)/core/board/NstBoardBandaiOekaKids.cpp \
	$(OBJDIR)/core/board/NstBoardBenshengBs5.cpp \
	$(OBJDIR)/core/board/NstBoardBmc110in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmc1200in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmc150in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmc15in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmc20in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmc21in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmc22Games.cpp \
	$(OBJDIR)/core/board/NstBoardBmc31in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmc35in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmc36in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmc64in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmc72in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmc76in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmc800in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmc8157.cpp \
	$(OBJDIR)/core/board/NstBoardBmc9999999in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmcA65as.cpp \
	$(OBJDIR)/core/board/NstBoardBmcBallgames11in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmcCh001.cpp \
	$(OBJDIR)/core/board/NstBoardBmcCtc65.cpp \
	$(OBJDIR)/core/board/NstBoardBmcFamily4646B.cpp \
	$(OBJDIR)/core/board/NstBoardBmcFk23c.cpp \
	$(OBJDIR)/core/board/NstBoardBmcGamestarA.cpp \
	$(OBJDIR)/core/board/NstBoardBmcGamestarB.cpp \
	$(OBJDIR)/core/board/NstBoardBmcGolden190in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmcGoldenCard6in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmcGoldenGame260in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmcHero.cpp \
	$(OBJDIR)/core/board/NstBoardBmcMarioParty7in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmcNovelDiamond.cpp \
	$(OBJDIR)/core/board/NstBoardBmcPowerjoy84in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmcResetBased4in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmcSuper22Games.cpp \
	$(OBJDIR)/core/board/NstBoardBmcSuper24in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmcSuper40in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmcSuper700in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmcSuperBig7in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmcSuperGun20in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmcSuperHiK300in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmcSuperHiK4in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmcSuperVision16in1.cpp \
	$(OBJDIR)/core/board/NstBoardBmcT262.cpp \
	$(OBJDIR)/core/board/NstBoardBmcVrc4.cpp \
	$(OBJDIR)/core/board/NstBoardBmcVt5201.cpp \
	$(OBJDIR)/core/board/NstBoardBmcY2k64in1.cpp \
	$(OBJDIR)/core/board/NstBoardBtl2708.cpp \
	$(OBJDIR)/core/board/NstBoardBtl6035052.cpp \
	$(OBJDIR)/core/board/NstBoardBtlAx5705.cpp \
	$(OBJDIR)/core/board/NstBoardBtlDragonNinja.cpp \
	$(OBJDIR)/core/board/NstBoardBtlGeniusMerioBros.cpp \
	$(OBJDIR)/core/board/NstBoardBtlMarioBaby.cpp \
	$(OBJDIR)/core/board/NstBoardBtlPikachuY2k.cpp \
	$(OBJDIR)/core/board/NstBoardBtlShuiGuanPipe.cpp \
	$(OBJDIR)/core/board/NstBoardBtlSmb2a.cpp \
	$(OBJDIR)/core/board/NstBoardBtlSmb2b.cpp \
	$(OBJDIR)/core/board/NstBoardBtlSmb2c.cpp \
	$(OBJDIR)/core/board/NstBoardBtlSmb3.cpp \
	$(OBJDIR)/core/board/NstBoardBtlSuperBros11.cpp \
	$(OBJDIR)/core/board/NstBoardBtlT230.cpp \
	$(OBJDIR)/core/board/NstBoardBtlTobidaseDaisakusen.cpp \
	$(OBJDIR)/core/board/NstBoardBxRom.cpp \
	$(OBJDIR)/core/board/NstBoardCaltron.cpp \
	$(OBJDIR)/core/board/NstBoardCamerica.cpp \
	$(OBJDIR)/core/board/NstBoardCneDecathlon.cpp \
	$(OBJDIR)/core/board/NstBoardCnePsb.cpp \
	$(OBJDIR)/core/board/NstBoardCneShlz.cpp \
	$(OBJDIR)/core/board/NstBoardCony.cpp \
	$(OBJDIR)/core/board/NstBoard.cpp \
	$(OBJDIR)/core/board/NstBoardCxRom.cpp \
	$(OBJDIR)/core/board/NstBoardDiscrete.cpp \
	$(OBJDIR)/core/board/NstBoardDreamtech.cpp \
	$(OBJDIR)/core/board/NstBoardEvent.cpp \
	$(OBJDIR)/core/board/NstBoardFb.cpp \
	$(OBJDIR)/core/board/NstBoardFfe.cpp \
	$(OBJDIR)/core/board/NstBoardFujiya.cpp \
	$(OBJDIR)/core/board/NstBoardFukutake.cpp \
	$(OBJDIR)/core/board/NstBoardFutureMedia.cpp \
	$(OBJDIR)/core/board/NstBoardGouder.cpp \
	$(OBJDIR)/core/board/NstBoardGxRom.cpp \
	$(OBJDIR)/core/board/NstBoardHenggedianzi.cpp \
	$(OBJDIR)/core/board/NstBoardHes.cpp \
	$(OBJDIR)/core/board/NstBoardHosenkan.cpp \
	$(OBJDIR)/core/board/NstBoardInlNsf.cpp \
	$(OBJDIR)/core/board/NstBoardIremG101.cpp \
	$(OBJDIR)/core/board/NstBoardIremH3001.cpp \
	$(OBJDIR)/core/board/NstBoardIremHolyDiver.cpp \
	$(OBJDIR)/core/board/NstBoardIremKaiketsu.cpp \
	$(OBJDIR)/core/board/NstBoardIremLrog017.cpp \
	$(OBJDIR)/core/board/NstBoardJalecoJf11.cpp \
	$(OBJDIR)/core/board/NstBoardJalecoJf13.cpp \
	$(OBJDIR)/core/board/NstBoardJalecoJf16.cpp \
	$(OBJDIR)/core/board/NstBoardJalecoJf17.cpp \
	$(OBJDIR)/core/board/NstBoardJalecoJf19.cpp \
	$(OBJDIR)/core/board/NstBoardJalecoSs88006.cpp \
	$(OBJDIR)/core/board/NstBoardJyCompany.cpp \
	$(OBJDIR)/core/board/NstBoardKaiser.cpp \
	$(OBJDIR)/core/board/NstBoardKasing.cpp \
	$(OBJDIR)/core/board/NstBoardKayH2288.cpp \
	$(OBJDIR)/core/board/NstBoardKayPandaPrince.cpp \
	$(OBJDIR)/core/board/NstBoardKonamiVrc1.cpp \
	$(OBJDIR)/core/board/NstBoardKonamiVrc2.cpp \
	$(OBJDIR)/core/board/NstBoardKonamiVrc3.cpp \
	$(OBJDIR)/core/board/NstBoardKonamiVrc4.cpp \
	$(OBJDIR)/core/board/NstBoardKonamiVrc6.cpp \
	$(OBJDIR)/core/board/NstBoardKonamiVrc7.cpp \
	$(OBJDIR)/core/board/NstBoardKonamiVsSystem.cpp \
	$(OBJDIR)/core/board/NstBoardMagicKidGoogoo.cpp \
	$(OBJDIR)/core/board/NstBoardMagicSeries.cpp \
	$(OBJDIR)/core/board/NstBoardMmc1.cpp \
	$(OBJDIR)/core/board/NstBoardMmc2.cpp \
	$(OBJDIR)/core/board/NstBoardMmc3.cpp \
	$(OBJDIR)/core/board/NstBoardMmc4.cpp \
	$(OBJDIR)/core/board/NstBoardMmc5.cpp \
	$(OBJDIR)/core/board/NstBoardMmc6.cpp \
	$(OBJDIR)/core/board/NstBoardNamcot163.cpp \
	$(OBJDIR)/core/board/NstBoardNamcot175.cpp \
	$(OBJDIR)/core/board/NstBoardNamcot340.cpp \
	$(OBJDIR)/core/board/NstBoardNamcot34xx.cpp \
	$(OBJDIR)/core/board/NstBoardNanjing.cpp \
	$(OBJDIR)/core/board/NstBoardNihon.cpp \
	$(OBJDIR)/core/board/NstBoardNitra.cpp \
	$(OBJDIR)/core/board/NstBoardNtdec.cpp \
	$(OBJDIR)/core/board/NstBoardOpenCorp.cpp \
	$(OBJDIR)/core/board/NstBoardQj.cpp \
	$(OBJDIR)/core/board/NstBoardRcm.cpp \
	$(OBJDIR)/core/board/NstBoardRexSoftDb5z.cpp \
	$(OBJDIR)/core/board/NstBoardRexSoftSl1632.cpp \
	$(OBJDIR)/core/board/NstBoardRumbleStation.cpp \
	$(OBJDIR)/core/board/NstBoardSachen74x374.cpp \
	$(OBJDIR)/core/board/NstBoardSachenS8259.cpp \
	$(OBJDIR)/core/board/NstBoardSachenSa0036.cpp \
	$(OBJDIR)/core/board/NstBoardSachenSa0037.cpp \
	$(OBJDIR)/core/board/NstBoardSachenSa72007.cpp \
	$(OBJDIR)/core/board/NstBoardSachenSa72008.cpp \
	$(OBJDIR)/core/board/NstBoardSachenStreetHeroes.cpp \
	$(OBJDIR)/core/board/NstBoardSachenTca01.cpp \
	$(OBJDIR)/core/board/NstBoardSachenTcu.cpp \
	$(OBJDIR)/core/board/NstBoardSomeriTeamSl12.cpp \
	$(OBJDIR)/core/board/NstBoardSubor.cpp \
	$(OBJDIR)/core/board/NstBoardSunsoft1.cpp \
	$(OBJDIR)/core/board/NstBoardSunsoft2.cpp \
	$(OBJDIR)/core/board/NstBoardSunsoft3.cpp \
	$(OBJDIR)/core/board/NstBoardSunsoft4.cpp \
	$(OBJDIR)/core/board/NstBoardSunsoft5b.cpp \
	$(OBJDIR)/core/board/NstBoardSunsoftDcs.cpp \
	$(OBJDIR)/core/board/NstBoardSunsoftFme7.cpp \
	$(OBJDIR)/core/board/NstBoardSuperGameBoogerman.cpp \
	$(OBJDIR)/core/board/NstBoardSuperGameLionKing.cpp \
	$(OBJDIR)/core/board/NstBoardSuperGamePocahontas2.cpp \
	$(OBJDIR)/core/board/NstBoardTaitoTc0190fmc.cpp \
	$(OBJDIR)/core/board/NstBoardTaitoTc0190fmcPal16r4.cpp \
	$(OBJDIR)/core/board/NstBoardTaitoX1005.cpp \
	$(OBJDIR)/core/board/NstBoardTaitoX1017.cpp \
	$(OBJDIR)/core/board/NstBoardTengen.cpp \
	$(OBJDIR)/core/board/NstBoardTengenRambo1.cpp \
	$(OBJDIR)/core/board/NstBoardTxc.cpp \
	$(OBJDIR)/core/board/NstBoardTxcMxmdhtwo.cpp \
	$(OBJDIR)/core/board/NstBoardTxcPoliceman.cpp \
	$(OBJDIR)/core/board/NstBoardTxcTw.cpp \
	$(OBJDIR)/core/board/NstBoardTxRom.cpp \
	$(OBJDIR)/core/board/NstBoardUnlA9746.cpp \
	$(OBJDIR)/core/board/NstBoardUnlCc21.cpp \
	$(OBJDIR)/core/board/NstBoardUnlEdu2000.cpp \
	$(OBJDIR)/core/board/NstBoardUnlKingOfFighters96.cpp \
	$(OBJDIR)/core/board/NstBoardUnlKingOfFighters97.cpp \
	$(OBJDIR)/core/board/NstBoardUnlMortalKombat2.cpp \
	$(OBJDIR)/core/board/NstBoardUnlN625092.cpp \
	$(OBJDIR)/core/board/NstBoardUnlSuperFighter3.cpp \
	$(OBJDIR)/core/board/NstBoardUnlTf1201.cpp \
	$(OBJDIR)/core/board/NstBoardUnlWorldHero.cpp \
	$(OBJDIR)/core/board/NstBoardUnlXzy.cpp \
	$(OBJDIR)/core/board/NstBoardUxRom.cpp \
	$(OBJDIR)/core/board/NstBoardVsSystem.cpp \
	$(OBJDIR)/core/board/NstBoardWaixing.cpp \
	$(OBJDIR)/core/board/NstBoardWaixingFfv.cpp \
	$(OBJDIR)/core/board/NstBoardWaixingFs304.cpp \
	$(OBJDIR)/core/board/NstBoardWaixingPs2.cpp \
	$(OBJDIR)/core/board/NstBoardWaixingSecurity.cpp \
	$(OBJDIR)/core/board/NstBoardWaixingSgz.cpp \
	$(OBJDIR)/core/board/NstBoardWaixingSgzlz.cpp \
	$(OBJDIR)/core/board/NstBoardWaixingSh2.cpp \
	$(OBJDIR)/core/board/NstBoardWaixingZs.cpp \
	$(OBJDIR)/core/board/NstBoardWhirlwind.cpp \
	$(OBJDIR)/core/board/NstBoardZz.cpp \
	$(OBJDIR)/core/input/NstInpAdapter.cpp \
	$(OBJDIR)/core/input/NstInpBandaiHyperShot.cpp \
	$(OBJDIR)/core/input/NstInpBarcodeWorld.cpp \
	$(OBJDIR)/core/input/NstInpCrazyClimber.cpp \
	$(OBJDIR)/core/input/NstInpDoremikkoKeyboard.cpp \
	$(OBJDIR)/core/input/NstInpExcitingBoxing.cpp \
	$(OBJDIR)/core/input/NstInpFamilyKeyboard.cpp \
	$(OBJDIR)/core/input/NstInpFamilyTrainer.cpp \
	$(OBJDIR)/core/input/NstInpHoriTrack.cpp \
	$(OBJDIR)/core/input/NstInpKonamiHyperShot.cpp \
	$(OBJDIR)/core/input/NstInpMahjong.cpp \
	$(OBJDIR)/core/input/NstInpMouse.cpp \
	$(OBJDIR)/core/input/NstInpOekaKidsTablet.cpp \
	$(OBJDIR)/core/input/NstInpPachinko.cpp \
	$(OBJDIR)/core/input/NstInpPad.cpp \
	$(OBJDIR)/core/input/NstInpPaddle.cpp \
	$(OBJDIR)/core/input/NstInpPartyTap.cpp \
	$(OBJDIR)/core/input/NstInpPokkunMoguraa.cpp \
	$(OBJDIR)/core/input/NstInpPowerGlove.cpp \
	$(OBJDIR)/core/input/NstInpPowerPad.cpp \
	$(OBJDIR)/core/input/NstInpRob.cpp \
	$(OBJDIR)/core/input/NstInpSuborKeyboard.cpp \
	$(OBJDIR)/core/input/NstInpTopRider.cpp \
	$(OBJDIR)/core/input/NstInpTurboFile.cpp \
	$(OBJDIR)/core/input/NstInpZapper.cpp \
	$(OBJDIR)/core/vssystem/NstVsRbiBaseball.cpp \
	$(OBJDIR)/core/vssystem/NstVsSuperXevious.cpp \
	$(OBJDIR)/core/vssystem/NstVsSystem.cpp \
	$(OBJDIR)/core/vssystem/NstVsTkoBoxing.cpp \
	$(OBJDIR)/jg.cpp

# Object dirs
OBJDIRS := $(OBJDIR)/core/api \
	$(OBJDIR)/core/board \
	$(OBJDIR)/core/input \
	$(OBJDIR)/core/vssystem \
	$(OBJDIR)/nes_ntsc

# List of object files
OBJS := $(CXXSRCS:.cpp=.o)

# Core rules
$(OBJDIR)/core/%.o: $(SOURCEDIR)/core/%.cpp $(OBJDIR)/.tag
	$(CXX) $(CXXFLAGS) $(FLAGS) $(CPPFLAGS) $(DEFS) -c $< -o $@

# Shared Library rules
$(OBJDIR)/%.o: $(SOURCEDIR)/%.cpp $(OBJDIR)/.tag
	$(CXX) $(CXXFLAGS) $(CFLAGS_JG) $(FLAGS) -c $< -o $@

all: $(TARGET)

$(OBJDIR)/.tag:
	@mkdir -p -- $(sort $(OBJDIRS))
	@touch $@

$(TARGET): $(OBJS)
	@mkdir -p $(NAME)
	$(CXX) $^ $(LIBS) $(LDFLAGS) $(SHARED) -o $(NAME)/$(TARGET)
	@cp $(SOURCEDIR)/NstDatabase.xml $(NAME)/
	@cp $(SOURCEDIR)/palettes/CXA2025AS-1536.pal $(NAME)/

clean:
	rm -rf $(OBJDIR)/ $(NAME)/

install: all
	@mkdir -p $(DESTDIR)$(DOCDIR)
	@mkdir -p $(DESTDIR)$(DATADIR)/jollygood/$(NAME)
	@mkdir -p $(DESTDIR)$(LIBDIR)/jollygood
	cp $(NAME)/$(TARGET) $(DESTDIR)$(LIBDIR)/jollygood/
	cp $(NAME)/NstDatabase.xml $(DESTDIR)$(DATADIR)/jollygood/$(NAME)/
	cp $(NAME)/CXA2025AS-1536.pal $(DESTDIR)$(DATADIR)/jollygood/$(NAME)/
	cp $(SOURCEDIR)/COPYING $(DESTDIR)$(DOCDIR)
	cp $(SOURCEDIR)/README $(DESTDIR)$(DOCDIR)

install-strip: install
	strip $(DESTDIR)$(LIBDIR)/jollygood/$(TARGET)

uninstall:
	rm -rf $(DESTDIR)$(DOCDIR)
	rm -rf $(DESTDIR)$(DATADIR)/jollygood/$(NAME)
	rm -f $(DESTDIR)$(LIBDIR)/jollygood/$(TARGET)
